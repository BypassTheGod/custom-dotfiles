# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
 
EAPI=7
 
DESCRIPTION="Some words here"
MY_PV="dwm-custom"
HOMEPAGE="https://gitlab.com/BypassTheGod/custom-dwm"
SRC_URI="https://gitlab.com/BypassTheGod/custom-dwm/-/blob/master/dwm-custom.tar.gz"
 
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
 
DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
